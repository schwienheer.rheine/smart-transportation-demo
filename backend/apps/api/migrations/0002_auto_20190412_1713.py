# Generated by Django 2.2 on 2019-04-12 17:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transportation',
            old_name='secret',
            new_name='seed',
        ),
    ]
