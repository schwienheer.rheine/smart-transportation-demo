from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='transportation',
            name='tx_address',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
