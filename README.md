# smart-transportation-demo

Proof of Concept solution for transport companies to track vehicles with cargo with means of IoT components and IOTA. 
Developed by Edgica during the Odyssey Hackathon 2019, Groningen, the Netherlands.

## Description of Demo Scenario
- Transport company equips its vehicles with smart IoT components:  a) wirelessly connected IoT device (Gateway) that tracks the GPS coordinates of the vehicle, and  b) set of sensors (Sensors) connected to the Gateway to measure the state and leaks of the cargo (temperature, gas, radiation, and so on)
- Dispatcher of a transport company plans the transportation of a Cargo and specifies details about the following: the Gateway ID of the vehicle, Start and Endpoints of the route, Type and Volume of the cargo, contact details of the driver.
- After the record of the planned transportation is created, the backend generates the transportation's Keys to be used for recording to IOTA via MAM protocol.
- Dispatcher starts the planned transportation and changes its state to Active.
- The Gateway of each vehicle connects to the backend periodically and checks if there is any transportation in Active state that is assigned to it. And if positive, the device gets the Keys of the transportation from the backend and starts logging the data from the Gateway and Sensors (GPS coordinates, temperature, and so on) to IOTA in a structured form (transportation's Checkpoints).
- The backend periodically checks the iota_backend_api, receives the Checkpoints from IOTA, and stores (caches) it in the database. This data can be used by users of the system to implement various use cases. For example, the Dispatcher of the transport company can control the location and state of the vehicles. Or the Fire Service control room can get real-time and trusted information about vehicles with hazardous materials in the vicinity of an incident. 
- When the Dispatcher changes the state of active transportation to Finished, the backend verifies the Checkpoints to confirm a) the Cargo was actually delivered to the Endpoint (GPS coordinates verification), and b) the Cargo was transported safely, i.e., there were no incidents during the transportation, by analyzing the data from the Sensors (to be developed).



**WARNING** This is a demo project developed during the hackathon, so it does not include some important security aspects, such as the authorization of the roles in the backend, the backend API authorization, and so on.

## Components

### gateway_listener
Node.js daemon running on the Gateway, reads information from MQTT that contains data from the Sensors, and writes the data to IOTA using the MAM protocol. Seed and side key to write the data is fetched from the backend via API for each transportation. A separate restricted channel is created, and transportation data is recorded at fixed intervals (GPS, temperature, gas).

### arduino
The modules that implement the functionality of reading data from Sensors.

### iota_backend_api
Node.js server provides an API for retrieving data from IOTA in JSON format for the backend

### backend
API based on django restframework that provides end-points of active transportation and their current location to 3rd-party solutions. 
The Django web application to implement a demo UI of the Dispatcher od transport company.

## Installation instruction

### gateway_listener
To run gateway_listener you need to install Node.js 8+, install js dependencies and create .env file with parameters.
```
npm install
```
in the gateway_listener directory, then create .env file with these parameters:<br>

```bash
DEVICE_ID=fee312cf-c9eb-44be-94e8-0a93a6ac8a5f #your device id registered on the backend
BACKEND_URL=http://localhost:8000/api/transportation/control/
IOTA_PROVIDER=https://nodes.devnet.iota.org:443
IOTA_SEND_INTERVAL=15
MQTT_URL=mqtt://localhost
MQTT_USER=user
MQTT_PASSWORD=password
MQTT_PORT=1883
MQTT_SENSOR_TEMPERATURE=mqsens-out/5/2/1/0/0 # sensor id on device
MQTT_SENSOR_GPS=mqsens-out/6/0/1/0/49 # sensor id on device
MQTT_SENSOR_GAS=mqsens-out/5/3/1/0/37 # sensor id on device
```

### iota_backend_api
To run iota_backend_api you need to install Node.js 8+, install js dependencies and create .env file with parameters.
```
npm install
```
in the iota_backend_api directory, then create .env file with these parameters:<br>
```bash
IOTA_PROVIDER=https://nodes.devnet.iota.org:443
```

### backend
To run the backend, you need python 3.6+, PostgreSQL 9+ and install python dependencies, GOOGLE_MAP_API_KEY is used for the widget to select points on the map.
```bash
pip install -r requirements.txt
python manage.py migrate
GOOGLE_MAP_API_KEY=<your google api key> python manage.py runserver
```